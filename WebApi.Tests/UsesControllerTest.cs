using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApi.Controllers;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.Users;
using WebApi.Services;
using Xunit;
using Moq;

namespace WebApi.Tests
{
    public class UsersControllerTest
    {
        private readonly UsersController _controller;
        private readonly IUserService _userService;

        private readonly Mock<IUserService> _mockUserService;


        public UsersControllerTest()
        {
            _mockUserService = new Mock<IUserService>();
            _userService = _mockUserService.Object;

            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfile());
            });
            var mapper = mockMapper.CreateMapper();
            _controller = new UsersController(_userService, mapper);
        }

        [Fact]
        public async Task GetAll_ShouldReturnAllUsers()
        {
            // Arrange
            var users = new List<User>
            {
                new User { Id = 1, FirstName = "John", LastName = "Doe", Email = "john@example.com" },
                new User { Id = 2, FirstName = "Peter", LastName = "Chan", Email = "peter@example.com" },
            };

            // Act
            _mockUserService.Setup(x => x.GetAll()).Returns(users);
            var response = _controller.GetAll();

            // Assert
            Assert.IsType<OkObjectResult>(response);
        }

        [Fact]
        public async Task GetById_ShouldReturnUserById()
        {
            // Arrange
            var user = new User { Id = 1, FirstName = "John", LastName = "Doe", Email = "john@example.com" };
            _mockUserService.Setup(x => x.GetById(1)).Returns(user);
            
            // Act
            var response = _controller.GetById(1);

            // Assert
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.Equal(user, result.Value);
        }

        // [Fact]
        // public async Task Create_ShouldCreateUser()
        // {
        //     // Arrange
        //     var model = new CreateRequest { Name = "John Doe" };

        //     _userService.CreateAsync(model).Returns(new User { Id = 1, Name = "John Doe" });

        //     // Act
        //     var response = await _controller.Create(model);

        //     // Assert
        //     Assert.IsType<OkObjectResult>(response);
        //     var result = response as OkObjectResult;
        //     Assert.Equal(new { message = "User created" }, result.Value);
        // }

        // [Fact]
        // public async Task Update_ShouldUpdateUser()
        // {
        //     // Arrange
        //     var model = new UpdateRequest { Name = "John Doe" };

        //     _userService.UpdateAsync(1, model).Returns(new User { Id = 1, Name = "John Doe" });

        //     // Act
        //     var response = await _controller.Update(1, model);

        //     // Assert
        //     Assert.IsType<OkObjectResult>(response);
        //     var result = response as OkObjectResult;
        //     Assert.Equal(new { message = "User updated" }, result.Value);
        // }

        // [Fact]
        // public async Task Delete_ShouldDeleteUser()
        // {
        //     // Arrange
        //     _userService.DeleteAsync(1).Returns(true);

        //     // Act
        //     var response = await _controller.Delete(1);

        //     // Assert
        //     Assert.IsType<OkObjectResult>(response);
        //     var result = response as OkObjectResult;
        //     Assert.Equal(new { message = "User deleted" }, result.Value);
        // }
    }
}
