# Reporting Tool
## Method 1: Replace Crystal Report
微軟原生提供RDLC作為Crystal Report的替代方案 [Microsoft, RDLC](https://marketplace.visualstudio.com/items?itemName=ProBITools.MicrosoftRdlcReportDesignerforVisualStudio-18001): 但這個方案[不支援Linux](https://stackoverflow.com/questions/76445111/how-to-use-rdlc-report-that-could-released-on-linux).

另外我們也提供下方四種的替代方案（都有額外費用）。會建議這四個方案，主要著眼點在於：
1) 都有Crystal Report轉移工具，以及
2) Designer工具，並提供
3) ASP.NET Core SDK。

由上而下代表我方了解的受歡迎程度。包含:
* [Telerik](https://github.com/telerik/kendo-ui-core)
    * [Tutorial](https://www.youtube.com/watch?v=UF1mL6vzJDs)
    * [Migrate from Crystal Report](https://docs.telerik.com/reporting/importing-reports/crystal-reports-converter)
* [FastReport](https://github.com/FastReports/FastReport)
    * [Tutorial](https://youtu.be/N63dLQePnI4)
    * [Migrate from Crystal Report](https://www.fast-report.com/en/blog/show/import-reports-from-crystal-reports-to-fastreport-net/)
* [Devexpress](https://github.com/DevExpress)
    * [Tutorial](https://www.youtube.com/watch?v=bK5ErFe3-2o)
    * [Migrate from Crystal Report](https://github.com/DevExpress/Reporting.Import/)
* [Active report](https://github.com/activereports)
    * [Tutorial](https://www.youtube.com/watch?v=XGLxhnI3IIc)
    * [Migrate from Crystal Report](https://developer.mescius.com/blogs/convert-crystal-reports-to-activereports-a-leading-net-reporting-solution)

---
## Method 2: Keep Crystal Reporting Service on-premise
重構地端服務，更改為Restful API服務。負責收取雲端過來的資訊，通過對應資訊印出套表，架構如下圖:
![Architecture](./imgs/service-topology.png)