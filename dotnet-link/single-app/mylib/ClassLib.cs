﻿using System;
using System.Text;

namespace mylib;
public class ClassLib
{
    public string call (string caller) {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine($"Hello from {caller}");
        sb.AppendLine("Awesome!");
        return sb.ToString();
    }
}
