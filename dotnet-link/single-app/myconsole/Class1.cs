﻿using System;
using mylib;

namespace myconsole;
public class Class1
{
    public static void Main() {
        try {
            ClassLib cl = new ClassLib();
            Console.WriteLine(cl.call("Shawn"));
        } catch (MissingMethodException e) {
            // Show the user that the DoSomething method cannot be called.
            Console.WriteLine("Unable to call the DoSomething method: {0}", e.Message);
        }
    }
}
