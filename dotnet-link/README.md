# dotnet-link
這個專案是用來展示如何產生客制dll檔案，並使用該檔案在專案中。這個專案的樹狀圖如下：
```
.
├── README.md
├── dotnet-link.sln
├── out.json
├── single-app
│   ├── Dockerfile
│   ├── k8s-manifests
│   ├── myconsole
│   ├── mylib
│   └── skaffold.yaml
└── skaffold.yaml
```
最外部的dotnet-link檔案夾，用來儲存sln檔案，以及skaffold.yaml。其中sln檔案的生成方式如下：
```
dotnet new sln 
dotnet sln add single-app/mylib/mylib.csproj
dotnet sln add single-app/myconsole/myconsole.csproj
```
skaffold.yaml主要通過require定義呼叫single-app下的skaffold.yaml

## single-app
這個主要用來定義一個服務，該服務在通過skaffold後會產生一個獨立的container image，並有自己的k8s-manifests部署檔案。下轄兩個子專案，mylib主要產生dll檔案，myconsole會使用產生出來的dll並進一步產生可以執行的myconsole.dll。我們使用pod 將其部署出來，確認服務有正常執行。

### mylib
這個專案，主要產生發布一個call的函數，提供給myconsole呼叫。這個子專案通過以下命令生成：
```
dotnet new classlib -o mylib
```
### myconsole

這個專案，主要產生dll的執行檔，主要entrypoint是Main函數。而專案內或是第三方dll連結說明，請見[本文件](https://learn.microsoft.com/zh-tw/dotnet/core/tools/dotnet-add-reference)。其中將mylib指定為reference，是通過下列命令：
```
dotnet add reference ../mylib/mylib.csproj
```

[如文件](https://learn.microsoft.com/en-us/previous-versions/visualstudio/visual-studio-2015/msbuild/common-msbuild-project-items?view=vs-2015&redirectedfrom=MSDN)，再於mylib.csproj中，新增<private>true</private> ，將mylib.dll設定為copy local。

最後完成的myconsole.csproj中，如下：
```
<Project Sdk="Microsoft.NET.Sdk">
  <ItemGroup>
    <ProjectReference Include="..\mylib\mylib.csproj">
      <private>true</private>
    </ProjectReference>
  </ItemGroup>
  <PropertyGroup>
    <OutputType>Exe</OutputType>
    <TargetFramework>net6.0</TargetFramework>
  </PropertyGroup>
</Project>
```

## 執行成果
修改single-app下的skaffold.yaml鏡像倉庫位置，以及single-app/k8s-manifests/pod.yaml下的鏡像倉庫位置，我們可以通過skaffold run來執行build + deploy兩件工作。
```
cd single-app
skaffold run
```

最後的部署我們使用Job，部署成功後，使用下方指令，應該可以看到myconsole正確的引用mylib裡的String而產生console output
```
kubectl logs -l job-name=myconsole
Hello from Shawn
Awesome!
```