# Vue3 JS (or) Nuxt3 JS + .NET 6 + PostgreSQL
這個專案提供開發者一個基於Vue3 JS (or) Nuxt3 JS + .NET 6 + PostgreSQL (e.g AlloyDB) 的前後端開發範例，用來操作使用者資料的CRUD操作。
可控制的方式包含：
• Create User 
• List User 
• Update User 
• Delete User 
• View User

## 部署方式：
進入kubernetes-manifests檔案，先修改appsetting-secret裡，Postgresql DB的位置，記得使用base64加密後儲存到YAML中。
使用以下指令，打包並部署dotnet-6-postgresql-crud-application的後台服務，
```
skaffold run
```
成功部署完畢後，使用以下命令檢查服務發布的位置：
```
kubectl get gateway,svc

NAME                                                   CLASS                            ADDRESS          PROGRAMMED   AGE
gateway.gateway.networking.k8s.io/dotnetcrud-gateway   gke-l7-global-external-managed   34.111.230.174   True         5d16h

NAME                    TYPE           CLUSTER-IP        EXTERNAL-IP      PORT(S)        AGE
service/dotnet-crud     LoadBalancer   192.168.129.176   104.199.246.61   80:31896/TCP   5d23h
```
如果有設定Gateway API與憑證的捧油，請使用Gateway的IP，結合憑證上的Domain Name，設定到Cloud DNS上，完成憑證的簽署（需要約10~20分鐘）。對於測試而不想搞憑證的捧油們，我們也可以直接使用service/dotnet-crud這種L4 LoadBalancer 的IP位置。

將上面步驟的Domain Name or L4 LB IP的位置，更新到以下 (2擇1 或是都更新也可)
1. vue3-crud-application裡的service/UserDataService.js 以及
2. nuxt-app/.env裡，定義一個 NUXT_BACKENDFQDN 變數，並加上http or https的前綴字 e.g. NUXT_BACKENDFQDN="https://dotnetcrud.shawnk8s.com"

並在每個目錄下，使用以下指令進行打包與部署工作:
```
skaffold run
```

## 結果:
部署完畢後，使用kubectl get svc 找出 nuxt-app or vue-user-crud的位置，打開瀏覽器
```
kubectl get svc

NAME            TYPE           CLUSTER-IP        EXTERNAL-IP      PORT(S)        AGE
nuxt-app        LoadBalancer   192.168.129.77    35.221.139.152   80:32107/TCP   5d11h
vue-user-crud   LoadBalancer   192.168.129.28    34.81.182.142    80:31778/TCP   13d
```

### VUE 3
vue-user-crud服務的頁面：

![vue3-viewpage](imgs/vue3-viewpage.png)
![vue3-addpage](imgs/vue3-addpage.png)

### Nuxt-3
nuxt-app服務的頁面：
![nuxt3-viewpage](imgs/nuxt3-viewpage.png)
![nuxt3-addpage](imgs/nuxt3-addpage.png)


