# Nuxt 3 Minimal Starter

This is my first nuxt-js project sample, which is a CRUD update UI to modify database through a backend API (made by .NET core6). This project uses the following technical stack:
* [Nuxt 3](https://nuxt.com/docs/getting-started/introduction)
* [Vue-fire](https://vuefire.vuejs.org/guide/getting-started.html)
* [SSR](https://chrlschn.medium.com/nuxt-3-with-ssr-on-google-cloud-firebase-functions-2023-b80f7c4d4b4d)

## Setup
It can either be deployed on firebase or on GKE. The committed code uses kubernetes version. 

1. Please modify the image location to the registry address which you have the privilege in both skaffold.yaml & kubernetes-manifests/deployment.yaml. 

2. Then, simply type:
```
skaffold build -p cloudbuild --file-output ./out.json && skaffold deploy --build-artifacts ./out.json
```

## Use firebase
This project is also ready to be deployed on firebase. If you would like to test, please follow the following two steps:
1. Generate firebase.json:
Using the following command
```
firebase init hosting
```
Accept all the default configuration. After firebase.json is generated, modify it with the following:
```
{
  "functions": { 
    "source": ".output/server",
    "codebase": "nuxt",
    "runtime": "nodejs18"
  },
  "hosting": {
    "public": ".output/public",
    "ignore": [
      "firebase.json",
      "**/.*",
      "**/node_modules/**"
    ],
    "cleanUrls": true,
    "rewrites": [
      {
        "source": "**",
        "function": {
          "functionId": "server",
          "region": "us-central1"
        }
      },
      {
        "source": "/_nuxt(/.*)?",
        "destination": "/_nuxt"
      }
    ],
    "predeploy": [
      "cd .output/server && rm -rf node_modules && npm install"
    ]
  }
}
```

2. Apply vue-fire:
Replace nuxt.config.ts with the following config:
```
import { readFileSync } from 'node:fs'
import { resolve } from 'node:path'

const __dirname = new URL('.', import.meta.url).pathname
const vuefirePkg = JSON.parse(
  readFileSync(resolve(__dirname, 'node_modules/vuefire/package.json'), 'utf-8')
)
const nuxtVuefirePkg = JSON.parse(
  readFileSync(
    resolve(__dirname, 'node_modules/nuxt-vuefire/package.json'),
    'utf-8'
  )
)
// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  app: {
    head: {
      title: "Nuxt3 CRUD Application",
      meta: [
        { hid: "description", name: "description", content: "Nuxt3 CRUD Application" },
        { charset: "utf-8" },
        { name: "viewport", content: "width=device-width, initial-scale=1" },
        { name: "format-detection", content: "telephone=no" }
      ],
      link: [
        { rel: "icon", type: "image/x-icon", href: "/favicon.ico"},
        {
          rel: 'stylesheet',
          href: 'https://fonts.googleapis.com/css2?family=Roboto&display=swap',
        },
        {
          rel: 'stylesheet',
          href: 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css',
        }
      ],
      script: [
        {
          src: 'https://code.jquery.com/jquery-3.7.1.min.js',
          type: 'text/javascript',
        },
        {
          src: 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js',
          type: 'text/javascript',
        }
      ]
    }
  },
  runtimeConfig: {
    public: {
      backendFqdn: process.env.NUXT_BACKENDFQDN,
    }
  },
  modules: [
    'nuxt-vuefire',
  ],
  ssr: true,
  nitro: {
    preset: 'firebase',
    firebase: {
      gen: 2,
      nodeVersion: '18',
    },
  },  
  // [Please use firebase console to generate hosting app to retrieve the following parameters.]
  vuefire: {
    config: {
      apiKey: "",
      authDomain: "",
      projectId: "",
      storageBucket: "",
      messagingSenderId: "",
      appId: "",
      measurementId: ""      
    }
  }
})

```
3. [Setup your GOOGLE_APPLICATION_CREDENTIALS](https://firebase.google.com/docs/admin/setup)
Attach your serviceaccount key to GOOGLE_APPLICATION_CREDENTIALS in order to provide sufficient permission for firebase to deploy cloud function. 
