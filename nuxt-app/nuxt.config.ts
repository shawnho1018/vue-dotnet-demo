import { readFileSync } from 'node:fs'
import { resolve } from 'node:path'

const __dirname = new URL('.', import.meta.url).pathname
// const vuefirePkg = JSON.parse(
//   readFileSync(resolve(__dirname, 'node_modules/vuefire/package.json'), 'utf-8')
// )
// const nuxtVuefirePkg = JSON.parse(
//   readFileSync(
//     resolve(__dirname, 'node_modules/nuxt-vuefire/package.json'),
//     'utf-8'
//   )
// )
// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  app: {
    head: {
      title: "Nuxt3 CRUD Application",
      meta: [
        { hid: "description", name: "description", content: "Nuxt3 CRUD Application" },
        { charset: "utf-8" },
        { name: "viewport", content: "width=device-width, initial-scale=1" },
        { name: "format-detection", content: "telephone=no" }
      ],
      link: [
        { rel: "icon", type: "image/x-icon", href: "/favicon.ico"},
        {
          rel: 'stylesheet',
          href: 'https://fonts.googleapis.com/css2?family=Roboto&display=swap',
        },
        {
          rel: 'stylesheet',
          href: 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css',
        }
      ],
      script: [
        {
          src: 'https://code.jquery.com/jquery-3.7.1.min.js',
          type: 'text/javascript',
        },
        {
          src: 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js',
          type: 'text/javascript',
        }
      ]
    }
  },
  runtimeConfig: {
    public: {
      backendFqdn: process.env.NUXT_BACKENDFQDN,
    }
  },
  ssr: true,
  //nitro: {
    // preset: 'firebase',
    // firebase: {
    //   gen: 2,
    //   nodeVersion: '18',
    // },
  //}
})
