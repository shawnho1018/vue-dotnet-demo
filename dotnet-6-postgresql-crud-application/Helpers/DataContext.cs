namespace WebApi.Helpers;
// DataContext 類別是一個 Entity Framework Core 類別，它提供對資料庫的訪問。它在 Users 屬性中有一個 DbSet，它用於訪問使用者資料表。
// 這個物件會被UserService.cs所使用，而UserService裡的物件會被UsersController.cs使用，連結與使用者間的呼叫。
using Microsoft.EntityFrameworkCore;
using WebApi.Entities;
using System.IO;
public class DataContext : DbContext
{
    // 這個屬性用於讀取 appsettings.json 檔案中的配置
    protected readonly IConfiguration Configuration;

    // 這個建構子用於初始化 DataContext 物件
    public DataContext(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    // 這個方法用於配置資料庫連接
    protected override void OnConfiguring(DbContextOptionsBuilder options)
    {
        // [shawnho]: this part could customize the location of the appsettings.json
        var builder = new ConfigurationBuilder()
                      .SetBasePath(Directory.GetCurrentDirectory())
                      .AddJsonFile("appsettings.json");
                      //.AddJsonFile("secrets/appsettings.json");
        var config = builder.Build();
        // connect to postgres with connection string from app settings
        options.UseNpgsql(config.GetConnectionString("ApiDatabase"))
        .UseSnakeCaseNamingConvention();
    }
    // 這個屬性用於訪問使用者資料表
    public DbSet<User> Users { get; set; }
}