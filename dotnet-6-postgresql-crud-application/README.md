# This is a sample code for ASP .NET 6 Web API
這個程式碼主要是使用ASP .NET Core來做出一個User資料庫的CRUD API Server，並使用Entity Framework打造ORM與後端Postgresql介接。底下我們針對三個部分：
1. 部署方式，
2. 容器生成與部署 + 憑證簽署與發布，與
3. 程式主體 + 資料庫介接與自動更新。

## 部署方式
目前部署使用skaffold，可使用
```
skaffold run
```
或是
```
skaffold build --file-output ./out.json && skaffold deploy --build-artifacts ./out.json
```
部署到GKE內。




## 容器生成與部署：
在這個範例，容器的部署，主要是定義在skaffold.yaml中，通過skaffold deploy render kubernetes-manifests內所有的YAML，來產生實際部署的設定。
其中，程式中所需要用到的參數已經集中到兩個設定中：appsetting-secret.yaml 以及flyway-sql.yaml。
* appsetting-secret.yaml 主要是對於提供給flyway(DB automation) 以及 Entity Framework連線字串使用。由於包含資料庫的帳密，以Secret方式儲存。
* flyway-sql.yaml包含了資料庫schema變更/Insert等設定。
* deployment.yaml & service.yaml是標準的k8s 物件，包含livenessprobe。
* gatewayapi.yaml主要是通過Gateway + HttpRoute的方式，將服務提供TLS加密憑證。

### 憑證簽發
憑證簽發可以參考[本連結](https://cloud.google.com/kubernetes-engine/docs/how-to/secure-gateway)。目前使用的是[Google Managed Certificate(GMC)](https://cloud.google.com/kubernetes-engine/docs/how-to/secure-gateway#create-ssl)，這個的先決條件是需要有可以自行新增DNS Entry的權限。因為簽發時，GMC會檢查IP是否可以成功被指定的Domain Name(FQDN)所解析。將憑證指定到Gateway API的方式，請見gatewayapi.yaml內的Gateway物件。

如果沒有DNS新增Record的權限，我們也可以使用[自簽的方式取得憑證](https://cloud.google.com/kubernetes-engine/docs/how-to/secure-gateway#secure-using-secret)。簽發出的憑證，可以通過以下YAML檔方式，掛到Gateway物件中。
```
kind: Gateway
apiVersion: gateway.networking.k8s.io/v1beta1
metadata:
  name: external-http
spec:
  gatewayClassName: gke-l7-global-external-managed
  listeners:
  - name: https
    protocol: HTTPS
    port: 443
    tls:
      mode: Terminate
      certificateRefs:
      - name: store-example-com
```

## 程式主體：
### Program.cs: 
Program.cs 是 ASP.NET Core 應用程式的入口點。它負責建立 WebApplication 物件，並將其配置為使用 HealthCheck、Log、資料庫 Driver、CORS、控制器、AutoMapper 和應用程式服務。它還設定 HTTP 請求管道，並啟動應用程式。

### Controllers/: 
UserController.cs 是 ASP.NET Core 控制器，它提供對使用者資料的 CRUD 操作。它使用 IUserService 服務來訪問資料庫。
在 Get() 方法中，它使用 IUserService 服務的 GetAll() 方法來獲取所有使用者。
在 Post() 方法中，它使用 IUserService 服務的 Create() 方法來新增一個使用者。
在 Put() 方法中，它使用 IUserService 服務的 Update() 方法來更新一個使用者。
在 Delete() 方法中，它使用 IUserService 服務的 Delete() 方法來刪除一個使用者。

### Entities/
Entities 是 ASP.NET Core 應用程式中用於表示資料庫資料的類別。它使用 Entity Framework Core 庫來與資料庫建立 ORM 映射。在這個範例中，Entities 類別包括 User 類別，它用於表示使用者資料。User 類別具有以下屬性：

* Id：使用者編號
* Name：使用者姓名
* Email：使用者電子郵件地址
* Password：使用者密碼
* Entities 類別還包括 Role 類別，它用於表示使用者角色。Role 類別具有以下屬性：
    Id：角色編號
    Name：角色名稱

Entities 類別是用於表示資料庫資料的類別。它使用 Entity Framework Core 庫來與資料庫建立 ORM 映射。

### Models/Users
Models/Users 是 ASP.NET Core 應用程式中用於表示使用者資料的類別。它使用 Entity Framework Core 庫來與資料庫建立 ORM 映射。
在這個範例中，Models/Users 類別具有以下屬性：
* Id：使用者編號
* Name：使用者姓名
* Email：使用者電子郵件地址
* Password：使用者密碼

### Services/
UserService 類別實作了 IUserService 介面。它使用 DataContext 類別來訪問資料庫。它還使用 AutoMapper 來自動映射模型和實體。
* IUserService.cs：這個類別定義了對使用者資料的 CRUD 操作的接口。
* UserService.cs：這個類別實作了 IUserService 介面。它使用 DataContext 類別來訪問資料庫。

### Helpers/
* DataContext.cs：這個類別提供對資料庫的訪問。它使用 Entity Framework Core 庫來與資料庫建立 ORM 映射。
* ErrorHandlerMiddleware.cs：這個類別用於處理全球錯誤。它使用 Microsoft.AspNetCore.Diagnostics 庫來記錄和顯示錯誤。
* AppException.cs: 這個類別可以用於在應用程式中拋出自定義錯誤。它可以被用於表示各種錯誤
* AutoMapperProfile.cs：這個類別用於配置 AutoMapper。它使用 AutoMapper 庫來自動映射模型和實體。

## 資料庫版本自動化更新
這裡主要是通過將flyway以initContainer方式執行，來進行資料庫的自動更新或是創建。未來flyway也可以做資料庫的migration。