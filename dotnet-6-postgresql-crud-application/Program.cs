﻿using System.Text.Json.Serialization;
using WebApi.Helpers;
using WebApi.Services;
// 建立 WebApplication 物件
var builder = WebApplication.CreateBuilder(args);
// 將 HealthCheck 服務加入 DI 容器
builder.Services.AddHealthChecks();
// 將 Log 輸出到控制台
builder.Host.ConfigureLogging(logging =>
{
    logging.ClearProviders();
    logging.AddConsole();
});
// 將資料庫 Driver 資訊與 CORS 加入 DI 容器
{
    var services = builder.Services;
    var env = builder.Environment;
    services.AddDbContext<DataContext>();
    services.AddCors();
    // 將控制器加入 DI 容器
    services.AddControllers().AddJsonOptions(x =>
    {
        // serialize enums as strings in api responses (e.g. Role)
        x.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());

        // ignore omitted parameters on models to enable optional params (e.g. User update)
        x.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
    });
    // 將 AutoMapper 加入 DI 容器
    services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

    // 將應用程式服務加入 DI 容器
    services.AddScoped<IUserService, UserService>();
}
// 建立 WebApplication 物件
var app = builder.Build();
// 將 HealthCheck 路徑設定為 /healthz
app.MapHealthChecks("/healthz");

// 設定 HTTP 請求管道
{
    // 設定全域 CORS 政策
    app.UseCors(x => x
        .AllowAnyOrigin()
        .AllowAnyMethod()
        .AllowAnyHeader());

    // 使用 ErrorHandlerMiddleware 處理全球錯誤
    app.UseMiddleware<ErrorHandlerMiddleware>();
    // 將控制器映射到 HTTP 請求
    app.MapControllers();
}
// 啟動應用程式
app.Run("http://0.0.0.0:9080");