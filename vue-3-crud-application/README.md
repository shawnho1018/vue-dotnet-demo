# vue3 frontend
這個專案提供一個基於vue js的前端服務的prototyping。 使用本專案前，請預先部署dotnet-6-postgresql-crud-application專案中的後台API服務。
在這個專案中，我們通過兩個頁面，對於使用者資料庫，進行新增、刪除、修改作業。

## 部署方式
目前部署使用skaffold，可使用
```
skaffold run
```
或是
```
skaffold build --file-output ./out.json && skaffold deploy --build-artifacts ./out.json
```
部署到GKE內。

## 程式主體說明
### package.json
這個檔案是 Vue.js 的主程式碼，通過使用template 函數定義了應用程式的視覺外觀。template 函數使用 HTML 語法來定義應用程式的佈局，包括標題、導覽列和主要內容區。其次使用 script 函數定義了應用程式的程式碼。script 函數使用 JavaScript 語法來定義應用程式的功能，包括路由、資料模型和事件處理函數。最後，export default 是用來匯出一個 Vue.js 元件。這個元件可以被其他 Vue.js 元件使用。

### main.js
這個檔案是 Vue.js 應用程式的入口檔案。它匯入 App 元件、bootstrap 函式庫和 CSS 檔案，並使用 createApp 函數建立一個 Vue.js 應用程式。然後，它使用 use 函數將 router 注入到應用程式中，並使用 mount 函數將應用程式掛載到 DOM 中。

以下是這個檔案的程式碼解析：

import { createApp } from 'vue'：這個語句匯入 Vue.js 的 createApp 函數。
import App from './App.vue'：這個語句匯入 App 元件。
import 'bootstrap'：這個語句匯入 bootstrap 函式庫。
import 'bootstrap/dist/css/bootstrap.min.css'：這個語句匯入 bootstrap CSS 檔案。
import router from './router'：這個語句匯入 router。
createApp(App).use(router).mount('#app')：這個語句使用 createApp 函數建立一個 Vue.js 應用程式，並將 router 注入到應用程式中，然後將應用程式掛載到 DOM 中。

### router.js
這個檔案是 Vue.js 應用程式的路由器。它定義了應用程式中不同的頁面，以及當使用者訪問這些頁面時要顯示的內容。router.js 檔案使用 createWebHistory 函數來建立一個歷史記錄，以便使用者可以來回瀏覽不同的頁面。它還使用 createRouter 函數來建立一個路由器，並將 routes 陣列傳入其中。routes 陣列定義了應用程式中不同的頁面，以及當使用者訪問這些頁面時要顯示的內容。

在這個例子中，routes 陣列包含兩個頁面：Users 和 User。Users 頁面顯示所有使用者資料，而 User 頁面顯示特定使用者的資料。當使用者訪問 / 頁面時，路由器會顯示 Users 頁面。當使用者訪問 /user/:id 頁面時，路由器會顯示 User 頁面，其中 :id 是使用者的 ID。

### UserDataService.js
這個檔案是 Vue.js 應用程式的資料服務器。它通過axios 函式庫提供了一個 API 來存取資料庫中的使用者資料。
UserDataService 類別提供以下方法：
- retrieveAllUsers()：獲取所有使用者資料。
- retrieveUser(id)：獲取指定使用者資料。
- deleteUser(id)：刪除指定使用者資料。
- updateUser(id, user)：更新指定使用者資料。
- createUser(user)：新增使用者資料。

### Users.vue
這個檔案主要是繪製完整的資料瀏覽頁面，並針對現有資料提供編輯與刪除功能。

### User.vue
這個檔案主要是繪製新增使用者頁面，並提供新增使用者資訊的按鍵。
