# Infrastructure as code:
這個章節，主要提供了Terraform的腳本，可用來進行以下GCP專案下的資源部署，部署出的架構如下圖
![預計產生資源的拓墣圖](img/creation.png)
目前產生的腳本分為：
- GCP provider (00_providers.tf)
- 網路 (01_network.tf): 包含VPC, Subnet, Secondary subnets, Cloud Router，以及 NAT Gateway。目前預設是全VPC網段都可以通Internet (不適用於企業生產環境)
- GKE (02_gke.tf): 產生包含GKE Cluster與一個GKE Worker Node (n2-standard-8)，目前產生的GKE叢集會使用private node，但會有Internet facing的GKE Control Endpoint（不適用於企業生產環境）。
- AlloyDB (02_alloydb.tf)：產生一個2vcore, 16GRAM的AlloyDB，由於價格考量，目前設定不包含HA以及Read Replicas。
- Cloud Workstation (03_cloudworkstation.tf): 產生一組開發者機器，會和GKE節點與AlloyDB的VPC Service Connect在同個VPC，這些開發機特別適合Windows桌機的開發者。主要因為目前Windows機器上缺乏許多預設的指令 (e.g. skaffold, jq, kubectl, gcloud, docker等)
