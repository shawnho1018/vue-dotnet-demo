
resource "google_compute_global_address" "private_ip_alloc" {
  name          = "private-ip-address-demo"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  project       = var.project_id
  network       = google_compute_network.custom-network.name
}

resource "google_service_networking_connection" "vpc_connection" {
  network                 = google_compute_network.custom-network.name
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_alloc.name]
}

module "alloy-db" {
  source               = "GoogleCloudPlatform/alloy-db/google"
  cluster_id           = "alloydb-demo"
  cluster_location     = var.gcp_region
  project_id           = var.project_id
  cluster_labels       = {}
  cluster_display_name = "alloydb-demo"
  cluster_initial_user = {
    user     = "admin",
    password = "admin"
  }
  network_self_link = google_compute_network.custom-network.name

  automated_backup_policy = {
    location      = var.gcp_region
    backup_window = "1800s",
    enabled       = true,
    weekly_schedule = {
      days_of_week = ["FRIDAY"],
      start_times  = ["2:00:00:00", ]
    }
    quantity_based_retention_count = 1,
    time_based_retention_count     = null,
    labels = {
      test = "alloydb-cluster"
    },
  }
  primary_instance = {
    instance_id   = "primary-instance-1",
    instance_type = "PRIMARY",
    machine_cpu_count = 2,
    availability_type = "ZONAL"
  }
#   read_pool_instance = [
#     {
#       instance_id  = "read-instance-1",
#       display_name = "read-instance-1",
#     }
#   ]
  depends_on = [ google_service_networking_connection.vpc_connection, module.project-services ]
}