// Instance Name for activity tracking
output "cluster_name" {
  value = module.gke.name
}

// Instance Zone for activity tracking
output "cluster_region" {
  value       = module.gke.region
  description = "The GCP zones of the GKE worker nodes"
}


